# redo-hs

This is an implementation of D.J. Bernstein's [redo][redo] in Haskell.

# Usage

    redo target

[redo]: http://cr.yp.to/redo.html
