{-# LANGUAGE StandaloneDeriving #-}
module Main where

import           Control.Monad      (filterM, liftM)
import           Data.Map.Lazy      (fromList, insert, toList)
import           Data.Maybe         (listToMaybe)
import           Debug.Trace        (traceShow)
import           System.Directory   (doesFileExist, removeFile, renameFile)
import           System.Environment (getArgs, getEnvironment)
import           System.Exit        (ExitCode (..))
import           System.FilePath    (hasExtension, replaceBaseName,
                                     takeBaseName)
import           System.IO          (hPutStrLn, stderr)
import           System.Process     (CmdSpec (..), CreateProcess (..),
                                     StdStream (..), createProcess, shell,
                                     waitForProcess)

trace' :: Show a => a -> a
trace' a = traceShow a a

deriving instance Show CreateProcess
deriving instance Show StdStream
deriving instance Show CmdSpec

main :: IO ()
main = mapM_ redo =<< getArgs

redo :: String -> IO ()
redo target = maybe fileNotFound redo' =<< redoPath target
    where
        redo' :: FilePath -> IO ()
        redo' path = do
            ctx <- getEnvironment
            (_, _, _, process) <- processEnv ctx . trace' . shell . cmd $ path
            exit <- waitForProcess process
            case trace' exit of
                ExitSuccess -> renameFile tempFile target
                ExitFailure code -> do
                    hPutStrLn stderr $ failedWith code
                    removeFile tempFile

        fileNotFound = error $ "No .do file found for target " ++ target
        processEnv ctx c = createProcess c {env = envList ctx}
        cmd p = trace' $ unwords ["sh", p, "0", takeBaseName target,
                         tempFile, ">", tempFile]
        envList old = Just $ newEnv old
        newEnv = toList . insert "REDO_TARGET" target . fromList
        tempFile = target ++ "---redoing"
        failedWith e = "Redo exited with code " ++ show e ++ "."


redoPath :: FilePath
         -> IO (Maybe FilePath)
redoPath target = listToMaybe `liftM` filterM doesFileExist candidates
    where candidates   = (target ++ ".do") : dofiles
          dofiles      = map (++ ".do") files
          files        = [replaceBaseName target "default"
                         | hasExtension target]
